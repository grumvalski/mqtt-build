#! /bin/bash
#
# build.sh
# Copyright (C) 2019 grumvalski <grumvalski@hal>
#
# Distributed under terms of the MIT license.
#
set -e

SCRIPT_HOME="$(dirname "$(realpath "$0")")"

apt-get update 
apt-get install -y build-essential debhelper devscripts gcc git libssl-dev lintian make

git clone https://github.com/eclipse/paho.mqtt.c.git /tmp/paho.mqtt.c

cp ${SCRIPT_HOME}/debian/* /tmp/paho.mqtt.c/debian
cd /tmp/paho.mqtt.c


debuild -us -uc

cp ../libpahomqttc_*.deb $SCRIPT_HOME
